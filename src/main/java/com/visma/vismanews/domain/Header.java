package com.visma.vismanews.domain;

public class Header {

    private int id;
    private String title;

    public Header(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
