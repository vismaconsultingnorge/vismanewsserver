package com.visma.vismanews.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Article {

    private  int id;
    private String title;
    private String content;

    public Article() {
    }

    public Article(Header header, String content) {
        id = header.getId();
        title = header.getTitle();
        this.content = content;
    }

    public Article(int id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
