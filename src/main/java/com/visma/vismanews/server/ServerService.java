package com.visma.vismanews.server;

import com.visma.vismanews.services.ArticleResource;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import java.util.ArrayList;
import java.util.Collections;

public class ServerService extends Service<ServerConfiguration> {

    public static void main(String[] args) throws Exception {
        ArrayList<String> argList = new ArrayList<String>();
        argList.add("server");
        Collections.addAll(argList, args);
        new ServerService().run(argList.toArray(new String[argList.size()]));
    }

    @Override
    public void initialize(Bootstrap<ServerConfiguration> bootstrap) {
        bootstrap.setName("visma-news");
    }

    @Override
    public void run(ServerConfiguration serverConfiguration, Environment environment) throws Exception {
        environment.addResource(new ArticleResource());
    }
}
