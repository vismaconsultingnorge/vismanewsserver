package com.visma.vismanews.services;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.visma.vismanews.domain.Article;
import com.visma.vismanews.domain.Header;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api")
public class ArticleResource {

    private final List<Article> articles;

    public ArticleResource() {
        articles = new ArrayList<Article>();
        articles.add(new Article(1, "Baby boom!", "Etter mye om og men har Ragne endelig klart å bli gravid. " +
                "Vi i Visma News gratulerer den nybakte mammaen og den kommende drooiden med en sprudlende ny Android app!"));
        articles.add(new Article(2, "NM i hjemmebrygging", "Øl er en forbruksvare som Per har skjønt at det går ann å lage hjemme. I år" +
                " stakk han av med en gjev tittel som norgesmester i hjemmebrygg. Med opphav i Tyskland var vel dette som forventet. " +
                "Derimot kom dette som et sjokk for Øyvind Ølberg som ikke deltok\nVi grattulerer "));
        articles.add(new Article(3, "Faktureringsgrad", "Etter en hektisk uke med en røddag midt i er vår kjære økonomisjef observert løpende" +
                " mellom alle prosjektene etter å finne noen med lavere enn 80% faktureringsgrad.\n Til hans store forbauselse hadde Ingar og Alf samlet " +
                "store deler av firmaet til et kompetansehevende fagmøte noe som førte til en del lavere faktureringsgrad på enkelte individer. \n" +
                "Bent ble senere funnet på kontoret sitt i fosterstilling hvor han gynget fram og tilbake og pekte på Excel arket som indikerte et " +
                "dårligere resultat enn forventet."));
        articles.add(new Article(4, "100% page coverage?", "Ida Benedikte har nå lest gjennom HELE SCJP boken. Hun har noen løse tråder men har disse på t.wait(). " +
                "Forventer en t.join() om ikke så lenge." +
                "Vi i Visma News satser på en Hole in one på eksamen"));
    }

    @GET
    @Path("/headers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Header> getHeaders(@QueryParam("fromId") Integer fromId) {
        final int id = fromId == null ? 0 : fromId;
        return FluentIterable.from(articles)
                .filter(new Predicate<Article>() {
                    @Override
                    public boolean apply(Article article) {
                        return article.getId() > id;
                    }
                })
                .transform(new Function<Article, Header>() {
                    @Override
                    public Header apply(Article article) {
                        return new Header(article.getId(), article.getTitle());
                    }
                }).toList();
    }

    @GET
    @Path("/article/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArticle(@PathParam("id") final Integer id) {
        List<Article> filter = FluentIterable.from(articles)
                .filter(new Predicate<Article>() {
                    @Override
                    public boolean apply(Article article) {
                        return article.getId() == id;
                    }
                }).toList();
        if (filter.size() == 1) {
            return Response.ok(filter.get(0)).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path("/article")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveArticle(Article article) {
        Article newArticle = new Article(articles.size() + 1, article.getTitle(), article.getContent());
        articles.add(newArticle);
        return Response.seeOther(URI.create("/article/" + newArticle.getId())).build();
    }

}
